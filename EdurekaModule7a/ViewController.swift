//
//  ViewController.swift
//  EdurekaModule7a
//
//  Created by Jay on 22/02/18.
//  Copyright © 2018 the chalakas. All rights reserved.
//

import UIKit
import Moya

class ViewController: UIViewController
{
    var provider: MoyaProvider<MyService>!

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        _ = MoyaProvider<MyService>()
        testapi()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func testapi()
    {
        print("we are in test api")
        let provider = MoyaProvider<MyService>()
        provider.request(.showAccounts)
        {
            result in
            switch result
            {
                case
                let .success(moyaResponse):
                    let data = moyaResponse.data // Data, your JSON response is probably in here!
                    _ = moyaResponse.statusCode // Int - 200, 401, 500, etc
                    let jsonString = String(data: data, encoding:.utf8)
                    print("success - " + String(describing: jsonString))
                
                // do something in your app
                case
                .failure(_):
                    print("got an error man -")
                // TODO: handle the error == best. comment. ever.
            }
        }
    }


}

