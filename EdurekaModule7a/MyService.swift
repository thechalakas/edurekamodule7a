//
//  MyService.swift
//  EdurekaModule7a
//
//  Created by Jay on 22/02/18.
//  Copyright © 2018 the chalakas. All rights reserved.
//

import Foundation
import Moya

enum MyService
{
    case zen
    case showUser(id: Int)
    case showAccounts
}

// MARK: - TargetType Protocol Implementation
extension MyService: TargetType {
    var baseURL: URL { return URL(string: "http://simplewebapi1webapp1.azurewebsites.net")! }
    var path: String {
        switch self {
        case .zen:
            return "/zen"
            //http://simplewebapi1webapp1.azurewebsites.net/api/Headphones/5
        case .showUser(let id):
            return "/api/Headphones/\(id)"
            //http://simplewebapi1webapp1.azurewebsites.net/api/Headphones/
        case .showAccounts:
            return "/api/Headphones"
        }
    }
    var method: Moya.Method {
        switch self {
        case .zen, .showUser, .showAccounts:
            return .get
        }
    }
    var task: Task {
        switch self {
        case .zen, .showUser, .showAccounts: // Send no parameters
            return .requestPlain

        }
    }
    var sampleData: Data {
        switch self {
        case .zen:
            return "Half measures are as bad as nothing at all.".utf8Encoded
        case .showUser(let id):
            return "{\"id\": \(id), \"first_name\": \"Harry\", \"last_name\": \"Potter\"}".utf8Encoded
        case .showAccounts:
            // Provided you have a file named accounts.json in your bundle.
            guard let url = Bundle.main.url(forResource: "accounts", withExtension: "json"),
                let data = try? Data(contentsOf: url) else {
                    return Data()
            }
            return data
        }
    }
    var headers: [String: String]? {
        return ["Content-type": "application/json"]
    }
}

// MARK: - Helpers
private extension String {
    var urlEscaped: String {
        return addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
    }
    
    var utf8Encoded: Data {
        return data(using: .utf8)!
    }
}
